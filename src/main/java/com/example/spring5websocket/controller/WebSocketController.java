package com.example.spring5websocket.controller;

import com.example.spring5websocket.handler.Event;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;

/**
 * 状态码 101 在 WebSocket 握手过程中表示协议的切换。它指示服务器已同意使用 WebSocket 协议，并且客户端和服务器之间的连接已从 HTTP 切换到了 WebSocket。
 * 这使得双方可以以全双工的方式进行实时、持久的双向通信，而不必再使用传统的请求-响应模式。一旦握手完成并且状态码为 101，双方就可以开始在建立的 WebSocket 连接上交换消息，
 * 而不再受限于 HTTP 请求和响应的约束。这种通信方式特别适合需要实时互动的应用程序，如实时聊天、实时通知等。
 */
@ServerEndpoint("/event-emitter")
public class WebSocketController {

    private static final ObjectMapper json = new ObjectMapper();
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketController.class);

    @OnClose
    public void onClose(Session session) throws IOException {
        // WebSocket connection closes
        LOGGER.info("WebSocket connection closes");
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig endpointConfig) throws IOException {
        // Get session and WebSocket connection
        session.setMaxIdleTimeout(0);
        LOGGER.info("Get session and WebSocket connection");
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        // Handle new messages
        LOGGER.info("Handle new messages -> {}", message);
        // Send a response back to the client
        Event event = new Event(randomUUID().toString(), now().toString());
        session.getBasicRemote().sendText("服务器收到了前端请求进行回应，状态码101,全双工通讯: " + json.writeValueAsString(event));
        LOGGER.info("Service responseEvent -> {}", json.writeValueAsString(event));
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        // Do error handling here
        LOGGER.info("Do error handling here");
    }
}
